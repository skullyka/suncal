#!/usr/bin/env python3
import datetime


from dateutil.relativedelta import relativedelta

from CalGenerator import CalGenerator





def main():
    begin = datetime.datetime(2023,5,20)
    end = begin + relativedelta(years=10)
    cg = CalGenerator(begin, end)
    cg.generate_items()


if __name__ == "__main__":
    main()
