from AstroEvents import AstroEvents
from CalHandler import CalHandler


class CalGenerator:
    def __init__(self, begin=None, end=None):
        self.calhandler = CalHandler()
        self.begin = (begin if begin is not None else datetime.date.today())
        self.events = AstroEvents(begin=self.begin, end=end).astroevent_generator()

    @staticmethod
    def create_event_subject(events):
        subject = {}
        for k, v in events.items():
           time = v.strftime("%H:%M")
           if k == "noon":
               subject["noon"] = "☀️ " + time + " Solar noon"
           if k == "sunrise":
               subject["sunrise"] = "☀️↑ "+time+" Sunrise"
           if k == "sunset":
               subject["sunset"] = "☀️↓ " + time + " Sunset"
        return subject

    def generate_items(self):
        for day in self.events:
            subjects = self.create_event_subject(day)
            for i in day.items():
                if i[0] in subjects:
                    self.calhandler.create_event(subjects[i[0]], subjects[i[0]], i[1], i[1])

