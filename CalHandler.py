import os
import pickle

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request




class CalHandler:
    SCOPES = ['https://www.googleapis.com/auth/calendar']

    CREDENTIALS_FILE = '/home/skully/projects/suncal/credentials.json'

    def __init__(self):
        self.service = self.setup()

    def setup(self):
        creds = None
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    self.CREDENTIALS_FILE, self.SCOPES)
                creds = flow.run_local_server(port=0)

            # Save the credentials for the next run
            with open('token.pickle', 'wb') as token:
                pickle.dump(creds, token)

        service = build('calendar', 'v3', credentials=creds)
        return service

    def create_event(self, summary, description, start, end):
        # creates one hour event tomorrow 10 AM IST
        e = s = {}
        s["iso"] = start.isoformat()
        e["iso"] = end.isoformat()
        s["tz"] ="Europe/Budapest"
        e["tz"] = "Europe/Budapest"
        event_result = self.service.events().insert(calendarId='khj98d0jj4gd5mjvgflibo7bt0@group.calendar.google.com',
                                                    body={
                                                        "summary": summary,
                                                        "description": description,
                                                        "start": {"dateTime": s["iso"], "timeZone": s["tz"]},
                                                        "end": {"dateTime": e["iso"], "timeZone": e["tz"]},
                                                    }
                                                    ).execute()
        print("created event")
        print("id: ", event_result['id'])
        print("summary: ", event_result['summary'])
        print("starts at: ", event_result['start']['dateTime'])
        print("ends at: ", event_result['end']['dateTime'])
        return event_result
