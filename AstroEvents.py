import datetime

from astral.geocoder import lookup, database
from astral.sun import sun
from pymeeus.Epoch import Epoch
from pymeeus.Sun import Sun


class AstroEvents:
    def __init__(self, loc="Budapest", begin=None, end=None):
        self.loc = self.get_location(loc)
        self.begin = (begin if begin is not None else datetime.date.today())
        self.end = (end if end is not None else self.begin)

    @staticmethod
    def get_location(loc):
        return lookup(loc, database())

    def __date_generator(self):
        current = self.begin
        while current <= self.end:
            yield current
            current += datetime.timedelta(days=1)

    def astroevent_generator(self):
        for i in self.__date_generator():
            yield self.calculate_times(i)


    def calculate_times(self, date=None):
        date = (date if date is not None else self.begin)
        s = sun(self.loc.observer, date, tzinfo=self.loc.timezone)
        return s

    def calculate_solstice(self, date=None):
        date = (date if date is not None else self.begin)
        year = date.year
        targets = ["spring", "summer", "autumn", "winter"]

        solstices = {}
        for target in targets:
            # Get terrestrial time of given solstice for given year
            solstice_epoch = Sun.get_equinox_solstice(year, target=target)

            solstice_local = (solstice_epoch + Epoch.utc2local() / (24 * 60 * 60))

            sol_date = solstice_local.get_full_date()
            solstices[target] = datetime.date(sol_date[0], sol_date[1], sol_date[2])
        return solstices
